FROM alpine

USER root:root

ENV HOME=/root
ENV PATH=$HOME/.local/bin:$PATH
ENV USER=root
ENV LOGNAME=root
ENV HOSTNAME=alpine
ENV SHELL=/bin/bash

RUN apk update
RUN apk upgrade
RUN apk add bash make m4 tmux git mercurial gcc neovim go chicken ocaml opam man man-pages the_silver_searcher coreutils wget
RUN apk add glib glib-static glib-dev glib-doc glib-networking
RUN apk add ncurses ncurses-dev readline readline-dev lynx
RUN chicken-install -u
RUN chicken-install matchable breadline srfi-1 args
RUN apk add perl perl-yaml-libyaml perl-app-cpanminus
RUN cpanm Mojo
# RUN cpanm DDP  ## fails

RUN mkdir -p $HOME/.local/bin $HOME/.conf $HOME/.local/share
RUN cd $HOME && git clone https://gitlab.com/alewmoose/dotfiles.git .dotfiles
RUN cd $HOME && git clone https://gitlab.com/alewmoose/utils.git .utils
RUN cd $HOME/.utils && ./create-links && cd $HOME/.dotfiles && git checkout work && create-links -f

RUN nvim -c 'PlugInstall | qa'

ENTRYPOINT bash

